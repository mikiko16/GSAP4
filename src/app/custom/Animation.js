import { gsap, timeline, Timeline} from "gsap/gsap-core";


export default class Animation {

    constructor() {
        let planets = document.getElementsByClassName('dots');
        this._planets = planets;

        let scaleButton = document.getElementById('scale-button');
        this._scaleBtn = scaleButton;

        let positionButton = document.getElementById('position-button');
        this._positionBtn = positionButton;

        let stopButton = document.getElementById('stop-button');
        this._stopBtn = stopButton;

        this._tl = gsap.timeline({repeat: -1, repeatDelay: 0});

        this._scaleBtn.addEventListener('click', () => {

          if(this._tl.isActive()) {
            this._tl.restart();
            this._tl.clear();
          }
          else{
            this._tl.restart();
          }

            this._tl.to(".dots", {
                id: 'scaleStagger',
                duration: 1,
                ease: 'easeIn',
                scale:0,
                stagger: {
                  amount: 0.5
                }
            });

            this._tl.to(".dots", {
              id: 'scaleStagger',
              duration: 1,
              scale: 1, 
              ease: 'easeOut',
              stagger: {
                amount: 0.5
              }
          });            
        })

        this._positionBtn.addEventListener('click', () => {
          
          if(this._tl.isActive()) {
            this._tl.restart();
            this._tl.clear();
          }
          else{
            this._tl.restart();
          }

          this._tl.to(".dots", {
            id: 'positionStagger',
            duration: 0.5,
            scale: 1, 
            y: -60,
            ease: 'Power0.easeNone',
            stagger: {
              from:'edges',
              amount: 0.5
            }
          });

          this._tl.to(".dots", {
            id: 'positionStagger',
            duration: 0.5,
            scale: 1, 
            y: 0,
            ease: 'Power0.easeNone',
            stagger: {
              from:'edges',
              amount: 0.5
            }
          });
        });

        this._stopBtn.addEventListener('click', () =>{

          this._tl.restart();
          this._tl.pause();
        })
    }


}